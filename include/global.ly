\version "2.18.2"

\paper {
	#(set-paper-size "a4")          % paper size
	% ragged-last-bottom = ##f      % fill the page till bottom
	% page-count = 1                % force to render just in 1 page
	% between-system-padding = 0    % empty padding between lines
}

NoChords = {
	\override Score.MetronomeMark #'padding = #4
}
WithChords = {
	\override Score.MetronomeMark #'padding = #8
}

\header {
	title = \markup \center-align {
		\override #'(font-name . "Purisa")
		\fontsize #4 \bold
		"Free as a Bird"
	}
	subsubtitle = \markup \center-align {
		\override #'(font-name . "Arial")
		"As played by Louis Armstrong in New Orleans Function"
	}
	composer=\markup \center-align {
		\override #'(font-name . "Arial")
		"Mary Shindler"
	}
	opus = \markup \tiny {
		\override #'(font-name . "Arial")
		"(1842)"
	}
	tagline = \markup {
		\override #'(font-name . "Arial")
		\tiny \column {
			\fill-line { "Transcription" \italic { "Giuseppe Ricupero" } }
			\fill-line { "Updated" \italic { "14-06-2017 00.11" } }
		}
	}
}

global = {
	\time 4/4
	\key f \minor
	\tempo 4 = 60
	\set Score.skipBars = ##t
	\set Score.countPercentRepeats = ##t
	\set Staff.hairpinToBarline = ##f
}
