soprano = \relative c' {
	\set Staff.instrumentName = "soprano"

	%% A %%
	c1*4
	%% B %%
	f1*4
}
