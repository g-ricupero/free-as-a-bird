bass = \relative c {
	\set Staff.instrumentName = "Bass"

	%% A %%
	c1*4
	%% B %%
	f1*4
}
