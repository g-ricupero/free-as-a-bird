\include "include/global.ly"
\include "include/outline.ly"
\include "include/rhythm.ly"

\header {
	instrument = \markup \italic \normalsize {
		\override #'(font-name . "Arial")
		"Harmony"
	}
}

\score { <<
		\new ChordNames {
			\set chordChanges = ##t
			\harmonyR
		}
		\new Staff \relative c'' <<
			\clef treble
			\WithChords \global \rhythm \rhythmPO
		>>
	>>
	\layout {}
}
